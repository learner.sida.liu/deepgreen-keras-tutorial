import itertools

import keras
import matplotlib.pyplot as plt
import numpy as np
from keras.utils import to_categorical
from keras_drop_block import DropBlock2D
from sklearn.metrics import confusion_matrix, classification_report
from tensorflow.python.client import device_lib


def main(
        batch_size=128,
        epochs=1,
        val_frac=0.2,
):
    (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

    # Rescale input range: [0, 255] -> [0, 1]
    x_train = (x_train.astype('float32') / 255.)[..., np.newaxis]
    x_test = (x_test.astype('float32') / 255.)[..., np.newaxis]

    # Split off a validation set
    inds = np.random.permutation(len(x_train))
    split_ind = int(len(x_train) * (1 - val_frac))
    train_inds, val_inds = inds[:split_ind], inds[split_ind:]
    x_val, y_val = x_train[val_inds], y_train[val_inds]
    x_train, y_train = x_train[train_inds], y_train[train_inds]

    print(f'Train shape:      {x_train.shape}')
    print(f'Validation shape: {x_val.shape}')
    print(f'Test shape:       {x_test.shape}')

    # Convert integer class labels to one-hot encoded class vectors
    y_train, y_val, y_test = to_categorical(y_train), to_categorical(y_val), to_categorical(y_test)

    # Construct the neural network
    model = build_neural_network()
    model.compile(
        optimizer='adam',
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    model.summary()

    # Distribute the neural network over multiple GPUs if available.
    gpu_count = len(available_gpus())
    if gpu_count > 1:
        print(f"\n\nModel parallelized over {gpu_count} GPUs.\n\n")
        parallel_model = keras.utils.multi_gpu_model(model, gpus=gpu_count)
    else:
        print("\n\nModel not parallelized over GPUs.\n\n")
        parallel_model = model

    parallel_model.compile(
        optimizer='adam',
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )

    checkpoint = keras.callbacks.ModelCheckpoint(
        '../output/weights.h5',
        monitor='val_acc',
        save_weights_only=True,
        save_best_only=True,
    )

    parallel_model.fit(
        x_train,
        y_train,
        batch_size=batch_size,
        epochs=epochs,
        verbose=1,
        validation_data=(x_val, y_val),
        callbacks=[checkpoint],
    )

    parallel_model.load_weights('../output/weights.h5')
    score = parallel_model.evaluate(x_test, y_test, verbose=1, batch_size=batch_size)
    print(f'Test score:    {score[0]: .4f}')
    print(f'Test accuracy: {score[1] * 100.:.2f}')

    preds = parallel_model.predict(x_test, batch_size=batch_size)

    c = confusion_matrix(np.argmax(y_test, axis=-1), np.argmax(preds, axis=-1))
    plot_confusion_matrix(
        c,
        list(range(10)),
        normalize=False,
        output_path='../output',
    )
    plot_confusion_matrix(
        c,
        list(range(10)),
        normalize=True,
        output_path='../output',
    )

    print(
        classification_report(
            np.argmax(y_test, axis=-1),
            np.argmax(preds, axis=-1),
            target_names=[str(x) for x in range(10)],
        )
    )


def build_neural_network():
    """
    Constructs a simple convolutional neural network with 2 convolution blocks
    followed by a single dense block. Drop block and dropout are used for regularization.
    """
    model = keras.models.Sequential()
    model.add(DropBlock2D(input_shape=(28, 28, 1), block_size=7, keep_prob=0.8, name='Input-Dropout'))
    model.add(keras.layers.Conv2D(filters=64, kernel_size=3, activation='relu', padding='same', name='Conv-1'))
    model.add(keras.layers.MaxPool2D(pool_size=2, name='Pool-1'))
    model.add(DropBlock2D(block_size=5, keep_prob=0.8, name='Dropout-1'))
    model.add(keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu', padding='same', name='Conv-2'))
    model.add(keras.layers.MaxPool2D(pool_size=2, name='Pool-2'))
    model.add(DropBlock2D(block_size=3, keep_prob=0.8, name='Dropout-2'))
    model.add(keras.layers.Flatten(name='Flatten'))
    model.add(keras.layers.Dense(units=256, activation='relu', name='Dense'))
    model.add(keras.layers.Dropout(rate=0.2, name='Dense-Dropout'))
    model.add(keras.layers.Dense(units=10, activation='softmax', name='Softmax'))
    return model


def available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']


def plot_confusion_matrix(
        cm,
        classes,
        normalize=False,
        title='Confusion matrix',
        cmap='Blues',
        output_path='.',
):
    """
    Logs and plots a confusion matrix, e.g. text and image output.

    Adapted from:
        http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        tag = '_norm'
        print("Normalized confusion matrix:")
    else:
        tag = ''
        print('Confusion matrix:')
    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=plt.get_cmap(cmap))
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black",
        )
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.savefig(f'{output_path}/confusion{tag}.png')
    plt.close()


if __name__ == '__main__':
    main(batch_size=2 ** 14, epochs=200)
