# Tutorial: Deep Learning on the VACC


## Table of Contents
- [Accessing BlueMoon and DeepGreen](#accessing-bluemoon-and-deepgreen)
- [Clone This Repository](#clone-this-repository)
- [Getting Started](#getting-started)
- [Setting Up Your Python Environment](#setting-up-your-python-environment)
  - [Install Anaconda](#install-anaconda)
  - [Installing Deep Learning Packages](#installing-deep-learning-packages)
- [Deep Learning On DeepGreen](#deep-learning-on-deepgreen)
- [VACC File Systems](#vacc-file-systems)
- [Setting Up A SSH Key](#setting-up-a-ssh-key)
- [Additional Resources](#additional-resources)


## Accessing BlueMoon and DeepGreen
The only way to access DeepGreen is through BlueMoon, the VACCs CPU oriented HPC cluster.
The mosy common way to do this is via SSH, using your NetID and associated password:
```bash
ssh {your NetID}@bluemoon-user1.uvm.edu
{your NetID}@bluemoon-user1.uvm.edu's password: {your NetID password}
```

For example:
```bash
ssh cvanoort@bluemoon-user1.uvm.edu
cvanoort@bluemoon-user1.uvm.edu's password: {cvanoort's password}
```

Once you've successfully ssh-ed onto BlueMoon, then you can ssh onto DeepGreen:
```
ssh dg-user1
```
- Note 1: You do not need to specify a username and you will not need to provide a password.
- Note 2: BlueMoon and DeepGreen both have two user nodes. It usually does not matter which you use.
- Note 3: Any files that you create or place on BlueMoon or DeepGreen can be accessed from both.


## Clone This Repository
Before you get too far, you'll want to clone this repository onto the VACC so that you'll have access to the source code.
```bash
cd ~/scratch
git clone https://gitlab.com/compstorylab/deepgreen-keras-tutorial.git
```

## Getting Started
The easiest way to get started is to run
```
bash ~/scratch/deepgreen-keras-tutorial/setup_conda.sh
```
This will create a fresh Anaconda install, create a new conda environment
titled `py_36_dg`, and install the packages required to run the example
code contained in `src/`.

Following this, you will need to run `source ~/.bashrc` or create a new
ssh session in order to use your new python setup.

If you want more control over your setup, or you're just curious, then read on.


## Setting Up Your Python Environment
### Install Anaconda
Anaconda gives you more control over the python version and package
versions that you'll be using.
It also greatly simplifies the setup of common deep learning packages,
like Tensorflow, Keras, and Pytorch.

Setting up Anaconda:
```bash
cd ~/scratch
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -bfu -p ~/scratch/miniconda3
```

You can copy and paste the lines above into your terminal or just run the following:
```bash
bash ~/scratch/deepgreen-keras-tutorial/setup_conda.sh
```

### Installing Deep Learning Packages
Once you have a python environment, you'll need to install some packages
in order to start doing some deep learning.

If you did not run `setup_conda.sh` then you'll need to install some
dependencies here.
The easiest way to do this is `conda env create -f deepgreen-keras-tutorial/py_36_dg.yml`.
This creates a new Anaconda environment, similar to a Python virtual environment (venv),
and installs the requirements listed in `py_36_dg.yml`.

You can install additional packages using `conda install {package names}` or
```
source activate py_36_dg
pip install {package names}
```

## Deep Learning On DeepGreen
In the `src/` folder you will find two files, `submit.sh` and `train.py`.
`train.py` is an example script that shows how to used Keras to
parallelize the training of a simple image classification network across
multiple GPUs.
By default the script will attempt to use all available GPUs, and will
notify the user if 1 or less usable GPUs were detected.
This implementation uses data parallelism, which involves:
 - Each GPU recieves an identical copy of the network being trained
 - Batch is split into chunks, where each GPU processes one chunk
 - The gradients from each chunk are combined on the CPU
 - The model held by each GPU is updated synchronously
 - Repeat.
There are other ways the deep learning can be parallelized, see
[this paper](https://arxiv.org/abs/1802.09941) for more details.

DeepGreen is a shared computing resource, meaning that many researchers
and research groups can use one or more of it's nodes simultaneously.
The user nodes that you SSH into are not meant to perform serious number
crunching, so we use `submit.sh` to submit our job to the cluster scheduler.

DeepGreen jobs are managed by the [slurm scheduler](https://slurm.schedmd.com/).
Slurm submission scripts specify the amount of resources that a job
should be allocated, including node count, CPU count, GPU count, memory,
and wall clock time.
Following these specifications, the body of the job is composed of
arbitrary bash commands, including activating conda environments and
executing python scripts.

For example, `submit.sh` requests a single node, 32 CPU cores, 8 GPUs,
16GB of memory, and 10 min of runtime.
In the body of `submit.sh` we simply activate the conda environment that
has our dependencies installed, and call `train.py`.

We can submit our job to the scheduler using `sbatch submit.sh`.
We can check the status of the job queue using `squeue`, and we can
filter the results to show just our jobs using `squeue -u {username}`.

[This cheat sheet](https://www.chpc.utah.edu/presentations/SlurmCheatsheet.pdf)
provides an overview of the important slurm commands.

Only a subset of the available job configuration options are used in `submit.sh`.
In general, almost any option for the `sbatch` command can be included in
the header of your job script.
See the [`sbatch` man page](https://slurm.schedmd.com/sbatch.html) for
details about additional options.

In addition to batch jobs, you can also submit interactive jobs to the
scheduler using the `srun` command.
The DeepGreen docs have an [example usage of `srun`](https://wiki.uvm.edu/w/DeepGreenDocs#Submitting_an_interactive_job).

If you are familiar with the scheduler on BlueMoon, PBS/Torque, it should
be easy for you to get started with slurm.
There is a correspondence between the commands that are used to interact
with both of these schedulers, see [this cheat sheet](https://slurm.schedmd.com/rosetta.pdf)
for more details.


## VACC File Systems
There are three primary file systems on the VACC.
BlueMoon and DeepGreen share two of them, `gpfs1` and `gpfs2`, while one
is exclusive to DeepGreen, `gpfs3`.

- `gpfs1`:
  - Spinning disk storage
  - Data is backed up
  - Has file count and file size quotas
  - File count and file size quotas are more strict
- `gpfs2`:
  - Spinning disk storage
  - Data is not backed up
  - Has file count and file size quotas
  - File count and file size quotas are less strict
- `gpfs3`:
  - NVMe storage (much faster than `gpfs1` or `gpfs2`)
  - Data backed up (2x redundancy)
  - Does not have file count or file size quotas
  - Only available one DeepGreen

Your home directory is stored in `gpfs1`, your scratch directory is on
`gpfs2`, and when you are on DeepGreen you can store files at
`/gpfs3/scratch/{username}`.

It is recommended that you store most files on `gpfs2`, since it has
less strict quotas.
You may want to use `gpfs3` for intermediate results that are created by
your scripts, for performance reasons.
However, you should be sure to move those results to `gpfs1` or `gpfs2`
when your job is complete.
Since `gpfs3` does not currently have quotas, it is up to each DeepGreen
user to utilize the space reasonably.


## Setting Up A SSH Key
You might not want to type your username and password everytime you want to SSH onto the VACC.
Fortunately, there's a way to fix that!

If you do not already have an ssh key for your machine, you'll need to generate one:
```
ssh-keygen -t rsa
```

This will make an ssh config folder, a public key, and a private key:
```
~/.ssh/
    ~/.ssh/id_rsa
    ~/.ssh/id_rsa.pub
```

Next, you'll need to make and populate a ssh config file:
```
mkdir ~/.ssh
vim ~/.ssh/config
```

You'll want to populate the ssh config file with aliases for the BlueMoon user nodes:
```
Host vacc
    HostName bluemoon-user1.uvm.edu
    Port 22
    User {your NetID}

Host vacc2
    HostName bluemoon-user2.uvm.edu
    Port 22
    User {your NetID}
```

Finally, you need to send your public key to the VACC:
```
ssh-copy-id -i ~/.ssh/id_rsa.pub {your NetID}@bluemoon-user1.uvm.edu
ssh-copy-id -i ~/.ssh/id_rsa.pub {your NetID}@bluemoon-user2.uvm.edu
```
If everything is setup correctly, now you should be able to use `ssh vacc` and `ssh vacc2` to access the VACC without entering your password.

## Additional Resources:
 - [VACC Documentation](https://wiki.uvm.edu/index.php?title=VACCUserDocumentation)
 - [DeepGreen Documentation](https://wiki.uvm.edu/w/DeepGreenDocs)
 - [Tensorflow on the VACC](https://wiki.uvm.edu/w/VACCTensorFlow)