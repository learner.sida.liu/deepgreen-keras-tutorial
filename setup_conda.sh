#! /bin/bash

# Setup an Anaconda install in ~/scratch
cd ~/scratch
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -bfu -p ~/scratch/miniconda3
rm Miniconda3-latest-Linux-x86_64.sh

# Run conda init, which should add the conda executables to the PATH
~/scratch/miniconda3/bin/conda init

# Refresh the .bashrc so that we can use conda commands
source ~/.bashrc

# Update conda in case there is a newer version
conda update -n base -c defaults conda

# Create a new environment, py36dg, that should facilitate deep learning with GPUs
conda env create -f ~/scratch/deepgreen-keras-tutorial/py36dg.yml
